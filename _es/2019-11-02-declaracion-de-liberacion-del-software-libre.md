---
layout: post
wppost_name: declaracion-de-liberacion-del-software-libre
title: Declaración de Liberación del Software Libre
date: 2019-11-02
tags:
  - anonimato
  - privacidad
  - transfeminismos
  - patriarcado
  - conversatorio
  - software libre
image:  
  path: public/images/conversatorio2.png
description: "Otra comunidad de software libre apropiándose de la libertad, ¡en contra de las opresiones!"
---

> Otra comunidad de software libre apropiándose de la libertad, ¡en
> contra de las opresiones!

En el [conversatorio sobre software libre y
patriarcado](/2019/09/25/conversatorios-anonimato-software-libre-y-patriarcado/)
en el que nos encontramos desde varias colectivas (PIP, HLV, Las de
Sistemas), activistas y personas que se acercaron --¡muchas por primera
vez!-- nos surgieron estas cosas.  Probablemente a otres les hayan
surgido otras, no está de más aclarar que estas son las que nos llamaron
la atención a les que nos juntamos a procesar la memoria un par de
semanas después, con el ánimo de expandir la discusión, plantear puntas,
ir aportando a dejar registro, una memoria en torno a una serie de
discusiones que venimos teniendo hace unos años y que vemos actualizarse
en varios espacios, seguramente gracias al fortalecimiento de los
transfeminismos.

***

Una de las cosas definitorias y ¿definitivas? del Software Libre son las
gastadas cuatro libertades. Se dice por ahí que Stallman se las sacó de
la galera, pero vamos aprendiendo a desconfiar de los genios machistas e
incluso de las historias colectivas construidas en torno a algunos
héroes, personalismos y preferimos pensar que son el fruto de una
comunidad de muchas personas, pensando y actuando de forma similar en
distintos lugares. Pero ¿por qué están definidas así? ¿y por qué justo
*estas* y no otras? ¿no hay otras libertades posibles, quizás en
contradicción con las que tenemos? Las cuatro libertades son lo primero
que nos graban en la cabeza las ortodoxias del software libre, aunque
nadie se las acuerde exactamente, sabemos que están.  Nos recuerda un
poco a rebelión en la granja.  Las cuatro libertades al fin y al cabo
están definidas taxativa, axiológicamente.  Parecen las leyes de la
termodinámica, o las de la robótica, pero en un giro neolingüístico muy
yanqui se llaman libertades, como las papas fritas o los bombardeos.

De hecho, nos pasó eso mismo en el conversatorio. Hubo un momento en el
que un compañero pirata habló de código abierto. Otres, más
acostumbrades a fundamentalizar, digo, a alzar la bandera del Software
Libre, necesitamos volver a repasarlas. En definitiva, un ejercicio en
la práctica: ¿nos siguen sirviendo esos puntos básicos y fundantes?
¿Necesitamos otros?

Quizás el Software Libre nos demanda no sólo las libertades 0, 1, 2 y 3,
sino también la -1 (¿ni una menos?): ¿puede haber SL si hay cisexismos,
prácticas opresivas, fascistas? Después de todo, el SL, nuestros
\*hacktivismos, necesitan reconocer que las tecnologías son culturas,
son bien común. Una humanidad en la que no puede haber borramientos por
identidades de género o por pertenencia a colectivos subalternos,
agregamos. Pero seguiremos por este camino, párrafos arriba. O abajo,
según cómo decida leerse o escucharse este texto.

Volviendo a las Libertades del SL. La que nos genera problemas es la
primera, la cero.  Como se presentan como leyes naturales, antes del 0
no hay nada.  Nos hacían notar compas en México que se parece mucho a la
regla dorada que nos unta la filosofía kantiana.  Dice que en pos de
limitar el poder de lxs programadorxs sobre lxs usuarixs, tenemos la
libertad de usar el programa para lo que queramos.  Así de primera mano
suena potente, porque no nos pueden decir que el programa que era para
hacer planillas no lo vamos a poder usar para llevar las cuentas de
nuestra comuna.  Sin embargo nos damos cuenta ahora, después de años de
dificultades intentando compartir (evangelizar, en jerga paternalista)
con compañeres de otros activismos la potencia del software libre, que
era la más problemática también, porque ¿por qué pueden usar estos
mismos programas los imperios para matar y violentar, para controlar y
producir?  Nos dicen que para que no haya problemas, estratégicamente,
el software libre viene con una reciprocidad incorporada donde el
imperio entra en contradicción y colabora en las herramientas de su
propia destrucción.  Probablemente no lo digan así.  Las cuatro
libertades vienen con un sesgo cientificista (nótese la ironía) donde
por decreto fundante la tecnología es neutra.  Depende quién la use y
para qué, la tecnología no tiene la culpa de ser tan neutra.

Y ahí entramos en la rosca sin fondo de la desconstrucción de sentidos.
¿No será que cuando otros usan y colaboran en el desarrollo, no le
frotan y le imbuyen esas tendencias asesinas?  Porque es cierto que el
grueso del software libre que existe es desarrollado y financiado por el
imperio, colaborando en obtener las mejores herramientas para extracción
y control del valor.  ¿Piensan ellos en nuestra libertad 0?

¿No podemos tener programas hechos por nosotres, para nosotres, con
nuestras políticas incorporadas?  ¿Por qué consideramos a las
tecnologías como herramientas que se acoplan a nuestras manos y no como
seres compañeres, aliades?  ¿No será que encima heredamos esas
concepciones instrumentalistas de la ciencia y la técnica hegemónicas?
¡Fundemos un Frente de Liberación Cyborg!

Recordamos que hubo y hay intentos de redefinir las licencias, que son
las formas legales de garantizar esas otras leyes.  Sobre ellas se
cierne la crítica disciplinadora ortodoxa: "pero no es libre".
Licencias no violentas, más políticas que éticas, aunque también, que
previenen a otros de usar los programas para hacer daño.  Ni siquiera,
les criticamos constructivamente, de prevenir a otros de explotar y
expoliar.

En la deconstrucción de términos ortodoxos, aparecen cosas como
software, privativo, libre... En el binarismo de la informática, el
software es la contraposición al hardware. Los aparatos, los fierros,
los electrones, son aquello "material", "duro" y rudo.  Como lo que
piensan y hacen las computadoras no lo es, entonces es software.  Pero
no hay nada de blando en los sueños de las computadoras.  Y recordamos
en este punto, que en realidad esta explicación invisibiliza
profundamente que en los orígenes de la informática, las computadoras no
pensaban ni soñaban por su cuenta, eran ayudadas (programadas) por
mujeres porque era considerado por su gestión de ingenieros (otrora
fetos) un trabajo monótono, sin iniciativa propia y similar a la
operación de una central telefónica.  Un trabajo mal pago donde las
blandas y suaves eran las pibas que inventaron los lenguajes de
programación, descubriendo el primer "bug".

No hay metáforas en la historia de la informática.  El software eran las
trabajadoras.  Pero cuando este trabajo se valorizó, fue reemplazado por
la figura heroica del cisvarón excéntrico que tantos años privilegiara a
Stallman dentro del MIT.  Cuando hay apropiación, hay borramiento.

En otro binarismo, lo que no es libre en inglés se dice _proprietary_,
indicando no solo que tiene dueños, sino que es propiedad privada.  Por
estos lados, reflexionamos acerca de la adopción local de esos términos
relacionados con los conceptos de libertad.  En este caso, reproduciendo
quizás fielmente este estereotipo de "chongo excéntrico", a nuestro
referente regional le pintó traducirlo por "privativo", porque "priva de
libertades, cuando el software siempre tiene dueños aunque seamos
todos".  Y ahí renegamos, porque Enrique Chaparro durante años nos
enseñó esto en sus divertidos monólogos y pensábamos que no se sentaba a
hablar con nosotres porque no estábamos a la altura, pero era para que
no nos enteremos que tiene una hija a la que el anonimato y la
privacidad le sirvieron para nunca pagarle alimentos.

¿Y ahora que el software libre ya no es tan libre, cómo le decimos?
