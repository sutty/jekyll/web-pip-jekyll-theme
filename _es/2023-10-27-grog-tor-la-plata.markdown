---
title: Grog &amp; tor - La Plata
description: Evento de autodefensa digital en la plata
author: []
image:
  path: public/js6zghv7i3fp8d2yc1c4jf5wpk0j/photo_2023-11-10_19-32-21.jpg
  description: flyer grog &amp; tor 2023
categories: []
tags:
- grog &amp; tor
- autodefensa digital
draft: false
order: 41
layout: actividad
uuid: 5fb5cb1b-bda1-47d1-aa83-143533b0618b
render_with_liquid: false
usuaries:
- 27
created_at: 2023-10-27 00:00:00.000000000 +00:00
last_modified_at: 2023-11-10 22:34:50.111223739 +00:00
---

<p>👾 Este viernes 27/10 nos encontramos para aprender juntxs sobre autodefensas digitales / seguridad informática en los activismos 👾 <br><br>🐱 En coordinación con el Partido Interdimensional Pirata, revivimos el viejo Grog&amp;Tor: un encuentro en el que bajo la modalidad charla-taller intercambiamos conocimientos sobre autodefensa digital. La idea es compartir, probar, usar y difundir herramientas libres que aseguren y protejan nuestra privacidad en el ciberespacio.<br><br>😶‍🌫 En estos tiempos en que el Estado se transforma en espanto, tras años de normalizar la vigilancia digital y la manipulación de nuestros datos sensibles, nos parece piola juntarnos a pensar estrategias colectivas para nuestra vida en común en Internet.<br><br>🐨 Les invitamos entonces este viernes a las 17hs a encontrarnos con estas conversaciones en el centro cultural y político Awkache, casa de la Biblioteca Popular La Carpinchera. Si podés, traé algún dispositivo (compu, celu, etc.). Si no, con algo para merendar alcanza :P</p>