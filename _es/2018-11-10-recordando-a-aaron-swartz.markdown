---
layout: post
title: Recordando a Aaron Swartz
date: 2018-11-10
wppost_name: recordando-a-aaron-swartz
tags:
  - aaron swartz
  - declaración
  - cultura libre
description: "Recordamos 5 años del asesinato de Aaron Swartz"
image:  
  path: public/images/recordando-a-aaron-swartz.jpg
---


## Transformar la vulnerabilidad en alianza y resistencia

La muerte activa nuestras preguntas por la finitud de las cuerpas, los
sentidos del dolor y el placer, la vulnerabilidad de la vida propia y la
ajena... En el dolor que se comparte nos reconocemos mutuamente
vulnerables y en la necesidad mutua de contención nos encontramos para
formar alianzas emocionales, círculos que se abrazan para ser más
fuertes y resistir. A menudo, la forma que encontramos para lidiar con
el dolor y las preguntas sobre la finitud, es creando un culto.

## Sobre sostener cotidianamente las relaciones de sometimiento o las manos de las propias productoras

En las sociedades en las que vivimos se supone que no se puede organizar
ningún gobierno sin representantes, sin políticos profesionales y sin
punteros territoriales; en los lugares donde trabajamos se supone que no
puede llevarse a cabo la producción sin jefes, sin gerentes y sin
técnicos; en las universidades o profesorados donde nos formamos se
supone que no podríamos educarnos sin funcionarios administrativos y sin
profesores catedráticos; en los sindicatos en los que nos afiliamos se
supone que no podríamos defender nuestros derechos laborales sin
militantes profesionales, líderes gremiales o dirigentes. El límite es
la religión: no existiría nada si no fuera por la divinidad que creó
todo y que vela por el mantenimiento de nuestra existencia. Estamos tan
acostumbradas a delegar las decisiones que afectan nuestras vidas
cotidianas que nos resulta dificilísimo o imposible concebir movimientos
políticos y sociales sin la figura de referentes. Al sostener y
reproducir cotidianamente relaciones de asimetría en la referencia de la
autoridad de las voces, nos corremos de identificar la propiedad común
con la propiedad pública. En los límites de la gestión de lo público y
de la propiedad, concedemos a un grupo de funcionarios, que manejen y
decidan sobre los problemas cotidianos de nuestras vidas. Del otro lado
está la posibilidad de, antes que convivir como un programa potencial de
divinización, estatización o propiedad pública para que nada cambie y
todo se reforme, vivir la experiencia de la propiedad común en manos de
sus propias productoras.

## Recordar es olvidar

Aaron Swartz (igual que muchas de nosotras) padeció la confluencia de
dos momentos, la transición entre la sociedad de control y la sociedad
de dominio. La sociedad de control lo vigiló y criminalizó; la sociedad
de dominio los utilizó a él y a sus afectos, para adoctrinar a las
activistas. En esta forma de control de la cuerpa social, se tiene por
objetivo que ésta sobreviva dentro de unos estándares de humanidad que
legislan nuestra natalidad, nuestra salud, educación, deseos, formas de
habitar, nuestra sexo-afectividad, nuestro género y la formas de su
expresión. Aaron Swartz lo entendió todo a los 14 años y empezó de toque
a pensar en la memoria colectiva, por eso le gustaban las bibliotecas,
las wikis, documentar. Pero nuestra memoria es mala y cada vez que nos
acordamos de algo, en la incapacidad que tenemos de conocer el mundo que
nos rodea, inevitablemente nos olvidamos de algo. Hay información que se
pierde al pasar de la experiencia vivida en carne propia al lenguaje en
el que se codifica la memoria. Pero por suerte, en esos olvidos tambien
se generan nuevos significados...

A nosotras nos gusta pensar en Aaron Swartz como une aliade ([¿era
queer?, eso no lo dicen](http://www.aaronsw.com/weblog/notgay)) con le
que seguimos dialogando desde el pasado y el presente, no como un
referente. Por eso, para recuperar los nuevos significados que nos
habilita la mala memoria de la experiencia irremplazable de su humanidad
negada por el aparato represivo, queremos actualizar algunas ideas sobre
la cultura libre y la propiedad común. 

## La forma de disponibilizar y difundir el conocimiento activa el problema de los medios que toma esa forma, es decir, las condiciones que la hacen ser posible

Digitalizar y compartir online viene siendo una estrategia importante en
la práctica de las disidencias, los pueblos originarios y las activistas
de la cultura libre. Desde hace tiempo venimos usando esa estrategia
para preservar nuestros conocimientos y tradiciones en nuestros propios
términos y desde nuestras propias narrativas. Pero hay una tensión en
que por su parte, las instituciones culturales y los medios de
comunicación usen, vendan y/o difundan productos de nuestros
conocimientos como si les fuesen propios. En la medida en que no podemos
decidir sobre lo que pueden o no pueden hacer esas instituciones, cuando
por ejemplo: una marca de ropa pone de moda un patrón de diseño
tradicional de una cultura originaria, cada vez que se digitaliza un
archivo y se lo sube a internet, e incluso cada vez que se estabiliza
una línea genética de un organismo modificado genéticamente y se patenta
su genoma; se pierde la posibilidad de dejar la impronta de la memoria
de las que hicimos posible esa realidad.

No es solamente que ciertos conocimientos que pertenecen a nuestras
comunidades activistas caigan bajo el dominio de una “propiedad
intelectual”, se privaticen y se mercantilicen, es que todo el
imaginario en torno al patrimonio colectivo de las culturas vivas, ya es
un argumento para su categorización racional bajo los lineamientos del
mercado.

Las industrias culturales en su expansión vienen desarrollándose sobre
la apropiación del dominio público de los saberes de las que los hicimos
posibles con las fuerzas de nuestros trabajos. Es que a través de un
intrincado entramado de reglas e instituciones, se fue construyendo un
paradigma que tiende a conceder derechos de propiedad intelectual sobre
el conocimiento, la sensibilidad y los imaginarios de nuestras
colectivas y comunidades activistas. Así, en la medida en la que alguno
de nuestros saberes entra en el sistema de categorización
bio-administrativo de asignación, valoración, promoción y producción en
masa, su significado presente y futuro entra a estar en disputa. Por
eso, las románticas del acceso ilimitado esconden en la otra mano el
control de quien mueve los cables, el robo que es la cultura de los
medios deprivativos de información, de producción, del trabajo y del
conocimiento.

## El adoctrinamiento de las fuerzas represivas funciona exterminando nuestras cuerpas

Que todo esté disponible online libremente en internet es que pertenezca
a las corporaciones que son dueñas de la web: internet son cables. El
hecho de que tengamos que organizarnos en comunidades digitales
sistemáticamente, de forma concertada y --muchas veces hasta-- desde
distintas regiones del mundo, nos deja picando en el cachete de las
nalgas la certeza de que si internet fuera nuestra, sería más fácil que
estuviera llena de las cosas que para nosotras son importantes y nos
interesan. Estamos subrepresentadas y nuestros conocimientos, nuestras
formas de compartirlo y producirlo, son invisibilizadas
sistemáticamente, perseguidas y marginadas.

Pero a la crítica estratégica de la concepción tradicional de la cultura
libre para contribuir a visibilizar y difundir esta invisibilidad y
falta de representación online de los conocimientos de las comunidades
activistas (trans-feministas, disidentes, antihegemónicas,
originarias, putas, marikas, negras, tortas, gordas, entre otras
interseccionalidades) hace falta agregarle la identificación de otro
territorio de resistencia en internet y las tecnologías. Y a esto
chantarle la estratagema de transgredir la estructura que se nos
presenta como dada para reapropiarnos y resignificar las tecnologías que
se nos presentan autoritariamente como centralizadas. Es decir, que al
identificar el piso de una violencia simbólica e institucional atroz por
parte de las instituciones y regímenes de propiedad intelectual
capitalistas convencionales, es necesario que pensemos en nuestra
autodefensa.

## Decime tus contradicciones y te diré tu ubicación en la marcha

Como un mismo gesto, el del registro, hecho por las corporaciones
significa la posibilidad del modelo de reconciliar la contradicción
dialéctica del trabajo en un estado inmutable de restauración y
conservación permanentes del orden de las cosas (la extracción óptima de
acuerdo a los estándares internacionales del plusvalor); pero al mismo
tiempo, en las manos de las que resistimos, en el gesto desesperado de
solamente seguir existiendo mediante la posibilidad de dejar el registro
de nuestra impronta, puede significar un síntoma de retaguardia y hasta
de derrota.

## Si no la torrenteamos, la cultura se netflixea

Una moral legalista que exija los mismos derechos de usufructo de las
producciones culturales, intelectuales y tecnológicas que las licencias
de propiedad intelectual confieren, bajo un código de reglas compuestas
en un sistema homólogo de promoción, también constituye una estrategia
llena de contradicciones. Bajo el régimen de consagración intelectual
que nos domina, una vez que los conocimientos entran en obras autorales
o patentes, las comunidades que los sustentan, en su invisibilidad, son
excluidas de los beneficios sociales de los nuevos trabajos creativos y
de los avances tecnológicos que se producen. Resulta que nos dicen que
para poder seguir produciendo tenemos que adquirir las semillas
mejoradas, los medicamentos y los bienes culturales que circulan por el
mercado, o ser unas marginales que se quedan en la franja low-tech de
ser las explotadas que sostienen el progreso.

La cultura viva de los pueblos se elabora constantemente a partir del
uso, el intercambio, la reinterpretación y el ensamblaje de elementos de
culturas diversas. Esta tarea, en tanto no privativa de los bienes
comunes, tiene un sentido creativo, expresivo, liberador, ya que amplía
el patrimonio cultural, le da nuevos sentidos, lo somete a la crítica y
lo pone en relación con elementos de otras culturas.

Por eso enmarcamos el activismo pirata en la ampliación de bienes
comunes vivos y no de archivos ilustrados del conocimiento humano
universalizado-colonizado.  Se trata de remover, reapropiar, circular y
dar sentido a la cultura por fuera de los círculos de mercantilización
que nos invisibilizan y que asesinaron a Aaron Swartz hace 5 años.

[Leer el Manifiesto por la Guerrilla del Acceso Abierto](<https://endefensadelsl.org/guerrilla_del_acceso_abierto.html)
