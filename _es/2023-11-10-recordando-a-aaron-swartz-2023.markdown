---
title: Recordando a Aaron Swartz 2023
description: Evento recordando a aaron swartz 2023 por el pip
author:
- pip
image:
  path: public/rmfdsfoab8sq46177487khkif8ux/photo_2023-11-10_19-36-18.jpg
  description: flyer recordando a aaron swartz 2023
categories: []
tags:
- aaron swartz
- cultura libre
draft: false
order: 42
layout: actividad
uuid: d97b442c-40af-4109-bc93-6d73d48869e8
render_with_liquid: false
usuaries:
- 27
created_at: 2023-11-10 22:38:52.622084130 +00:00
last_modified_at: 2023-11-10 23:37:50.860422298 +00:00
---

<p><br></p><figure data-multimedia=""><img data-multimedia-inner="" src="public/ub67htwy6a29spm1rltyah6tcead/photo_2023-11-10_19-36-20.jpg" alt=""><figcaption>Recordando a Aaron Swartz</figcaption></figure><p><br>Este sábado 11 de noviembre de 12 a 20hs estaremos recordando a Aaron Swartz en C.A.O.S (av 7 y c. 78, Villa Elvira, La Plata), con actividades desde el Partido Interdimensional Pirata y amigxs.<br><br>Si venís temprano, vamos a estar almorzando juntxs, podés traer algo para compartir.<br><br>Este es un evento de cultura libre y activismo pirata y antifascista. Si querés participar ayudándonos a organizar, podés unirte a este <a href="https://t.me/+SYzqJoF3G7w6oAvs" rel="noopener" target="_blank">chat&nbsp;</a> o escribirnos a contacto@partidopirata.com.ar<br><br>Seguimos estos <a href="https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html" rel="noopener" target="_blank">códigos para compartir</a><br><br>Importante: El domingo 12 no habrá actividades, invitamos a visitar la feria del libro punk y derivadxs en la Cazona de Flores (Morón 2453, CABA)<br><br>Actividades:<br><br>* Huerta posible<br>* Radio y redes comunitarias<br>* Web distribuida y fediverso<br>* Taller textil<br>* Escaneá tu libro o fanzine</p>