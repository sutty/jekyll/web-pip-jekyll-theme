---
layout: actividad
wppost_name: conversatorio-anonimato-y-patriarcado
title: Segundo encuentro del Conversatorio sobre Anonimato, Software Libre y Patriarcado
date: 2019-11-02
tags:
  - anonimato
  - privacidad
  - transfeminismos
  - patriarcado
  - conversatorio
image:  
  path: public/images/conversatorio2.png
description: "Lo personal es político.  Lo técnico es político.  ¿Qué pasa cuando las herramientas y estrategias que activamos sirven para apañar machirulos, proteger trolls y abusadores y sostener el cistema que nos violenta?"
---

**Quiénes:** Partido Interdimensional Pirata, [Hacklab
Violeta](https://hlv.neocities.org/), [Tierra
Violeta](http://www.tierra-violeta.com.ar/), [Red Argentina de Género,
Ciencia y Tecnología](http://ragcyt.org.ar/), [Las de
Sistemas](http://lasdesistemas.org/)

**Lugar:** Tierra Violeta, Tacuarí 538 (CABA) (El espacio aun no es
accesible para usuaries de sillas de ruedas, ¡perdón!)

**Segundo Encuentro:** martes 5/11!

**Conversan:** Quienes quieran :) ¡Es un conversatorio!

**Compartimos:**

Cuando el anonimato es un privilegio, ¿qué otras herramientas nos
quedan? Nos juntamos a conversar sobre este tema, continuando el
encuentro que tuvimos el 1/10 sobre Software Libre, Anonimato y
Patriarcado.

Lo personal es político.  Lo técnico es político.  ¿Qué pasa cuando las
herramientas y estrategias que activamos sirven para apañar machirulos,
proteger trolls y abusadores y sostener el cistema que nos violenta?
Genealogías hackers en el Software Libre: "nuestro ídolo es un forro"
(¿?) ¿Y si todas esas herramientas estaban pensadas desde el principio
para sostener abusadores? ¿Podemos quedarnos tranquiles diciendo que las
herramientas son neutras y dependen de la mano que las use?  ¿Nos siguen
sirviendo las mismas herramientas y estrategias o necesitamos
revisarlas, inventar unas propias?

Nos juntamos a problematizarnos los hacktivismos frente y en contra de
los abusos, la trata, el abuso sexual infantil y desde los
hacktransfeminismos, pensando estrategias y herramientas existentes y
por venir P)

# Materiales para compartir

Algunos materiales de lectura para entrar en tema:

* [Declaración de Liberación del Software
  Libre](/2019/11/02/declaracion-de-liberacion-del-software-libre/) es
  un texto que escribimos a partir del primera encuentro.

* [Reconsiderando el anonimato en la era del
  narcisismo](https://utopia.partidopirata.com.ar/zines/reconsiderando_el_anonimato_en_la_era_del_narcisismo.html)

* [Códigos para compartir, hackear, piratear en
  libertad](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html)
  son los códigos que consensuamos guiar nuestras actividades.

* [¿Dónde está el feminismo en el
  ciberfeminismo?](https://utopia.partidopirata.com.ar/zines/donde_esta_el_feminismo_en_el_ciberfeminismo.html)

* [Manifiesto por Algoritmias
  Hackfeministas](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html)
