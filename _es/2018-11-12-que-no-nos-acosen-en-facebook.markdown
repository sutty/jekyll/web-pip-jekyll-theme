---
layout: post
title: Que no nos acosen por Facebook
date: 2018-11-12
wppost_name: que-no-nos-acosen-por-facebook
tags:
  - acoso
  - machitrolls
  - fuerzas represivas
  - facebook
description: "Exploramos las opciones de privacidad de Facebook"
image:  
  path: public/images/001_portada.png
---

Ya sabemos que **Facebook gana miles de millones de dólares** con todo
el contenido que subimos a su plataforma, por eso las piratas decimos
que **trabajamos para Facebook** P), pero además, por la cantidad de datos
que le proporcionamos y porque facebook no los cuida, **los machirulos y
las fuerzas represivas los usan para hostigarnos o vigilarnos**.

no vamos a decir que tenemos que cerrar nuestras cuentas (...o sí), así
que compartimos una guía para que nuestras cuentas sean **un poco más
privadas**.

Esto no asegura que no se pueda tener acceso a las cosas que publicamos
en Facebook, solo les dificulta el trabajo. especialmente las **fuerzas
represivas tienen acceso directo a los datos** de Facebook y otras
plataformas capitalistas.

![](/images/001_portada.png)
![](/images/002_tinfoil.png)
![](/images/003_privacidad.png)
![](/images/004_ubicacion_y_biografia.png)
![](/images/005_seguridad.png)
![](/images/006_contraportada.png)
