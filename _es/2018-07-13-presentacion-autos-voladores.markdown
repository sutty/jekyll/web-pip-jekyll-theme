---
layout: actividad
title: Presentación del libro "Sobre los autos voladores..." de David Graeber
date: 2018-07-13
wppost_name: presentacion-autos-voladores
tags:
  - utopías piratas
  - utopía pirata
  - utopia pirata
  - libros
  - david graeber
image:  
  path: public/images/autos_voladores.jpg
description: "El 27 de julio de 18 a 20hs. en la biblioteca popular Eduardo Martedí, Pasco 555 (CABA)"
---

# Qué vamos a hacer

Hace poco tiempo tradujimos y editamos un texto que se llama "Sobre los
autos voladores y la tasa decreciente de ganancia" de David Graeber.  Es
un texto que nos gustó aunque nos costó hacer entrar el título en una
tapita de 14x10cm y ahora queremos presentarlo.

Así que nos vamos a juntar en la [biblioteca popular Eduardo
Martedí](https://www.facebook.com/BP.EduardoMartedi), Pasco 555 (CABA),
el viernes 27 de julio de 18 a 20hs, a hablar un poco sobre qué es
[Utopía Pirata](https://utopia.partidopirata.com.ar/), nuestro proyecto
editorial, y de qué va esto de los autos voladores.

Si no leíste el libro, lo podés [descargar
acá](https://utopia.partidopirata.com.ar/sobre_los_autos_voladores_y_la_tasa_decreciente_de_ganancia.html).
Imprímitelo, o descargate FBReader desde
[F-Droid](https://wiki.partidopirata.com.ar/F-Droid) para leerlo en el
celular.  Si lo querés en papel,
[escribinos](https://partidopirata.com.ar/contacto.html)

También podés aprovechar los apuntes que salen más abajo, que te podés
descargar en [PDF listo para imprimir, cortar a la mitad, doblar y
compartir](/files/presentacion-autos-voladores-imposed.pdf) o en [ePub
para leer en el celular](/files/presentacion-autos-voladores.epub) o el
ebook.

Y si no te da para investigar qué es Utopía Pirata y leer todo esto,
vení a la presentación y nos conocemos.

No te olvides de traer un pendrive para compartir libros P)


# Qué es Utopía Pirata

Utopía Pirata, o a veces cuando nos vamos de mambo, Utopías Piratas, es
la barca editorial del Partido Interdimensional Pirata. La tarea que nos
auto-asignamos es la de leer, seleccionar, traducir si es necesario,
editar, publicar y difundir textos que enriquezcan discusiones y
prácticas que consideramos dentro de la esfera pirata y por ahí
expandirla hacia otras utopías.

Nos faltan un montón de temas por tocar.

Utopía es también un experimento editorial. Nos proponemos publicar, sí,
e incluso llevamos al papel, pero la apuesta como proyecto es la de ser
un proyecto editorial libre. Todas las herramientas que hacemos y usamos
son software libre, los formatos, los diseños, las plantillas y el
conocimiento que ponemos en este juego son libres.

Los textos, claro, son libres aunque a veces solo tácitamente libres P)

# Quién es David Graeber

Vinimos a darnos cuenta en los últimos días que [David
Graeber](https://es.wikipedia.org/wiki/David_Graeber) es nuestro
caballito de batalla para un montón de temas.  En [The Democracy
Project](https://www.epublibre.org/libro/detalle/11267) ya nos apuntaba
a Starhawk como formadora en democracia directa y toma de decisiones por
consenso, que luego publicaríamos como [Herramientas para la democracia
directa](https://utopia.partidopirata.com.ar/democracia_directa.html),
uno de los primeros textos de Utopía Pirata.  Luego re-publicamos [Las
nuevas
anarquistas](https://utopia.partidopirata.com.ar/las_nuevas_anarquistas.html),
un artículo donde analiza formas de activismo, aunque nunca nos animamos
a sacarlo en papel.

Y leímos [En deuda: los primeros 5000
años](https://www.epublibre.org/libro/detalle/7098), donde  habla sobre
los mecanismos capitalistas para mantenernos en deuda, desbarranca el
mito neo-liberal del origen del dinero y [La utopía de las
normas](https://www.epublibre.org/libro/detalle/35165), sobre el afán
burocratizante de esta etapa del capitalismo.

Y ahora estamos leyendo [Bullshit Jobs: A
theory](http://gen.lib.rus.ec/book/index.php?md5=C7F5757FBB776BCD22547B368309787E),
donde expande su hipótesis sobre [el auge de los laburos de
mierda](http://guerrillatranslation.com/2013/09/24/el-fenomeno-de-los-curros-inutiles/),
rejuveneciendo la discusión anti y post-trabajo y al que próximamente
editaremos.

El problema es que el chabón nos cae bien, pese a ser un antropólogo
gringo y blanco :P

# Apuntes

El artículo en inglés fue publicado en marzo de 2012 como [_Of Flying
Cars and the Declining Rate of Profit_ en The
Baffler](https://thebaffler.com/salvos/of-flying-cars-and-the-declining-rate-of-profit).

Arranca estableciendo la existencia de una decepción general sobre el
rumbo que siguió el desarrollo tecnológico en el último medio siglo e
identifica que esa decepción nos dejó preguntándonos qué pasó con los
autos voladores; lo que vendría a ser una especie de meme sobre lo que
le pedimos a una cultura que nos prometía un futuro encantador y
profundizó una distopía horrible. Entonces, si bien de entrada puede
sonar un poco _naïve_ este cuestionamiento sobre la inexistencia de los
autos voladores, detrás de esa pregunta se aloja una insatisfacción en
la que Graeber propone el gancho del texto.

Este cuestionamiento se contrapone a cierto optimismo tecnológico que
vendría a ser algo así como una forma de determinismo en la que mucha
gente parece asumir que vivimos en una era de innovación técnica sin
precedentes; pero que por otro lado, tiene la doble cara de reconocer
elementos de la estética _cyberpunk_ con la que venimos soñamos desde
chicas.

El texto sirve mucho para situar históricamente el problema del relato
sobre el futuro. Arranca diciendo que a todas aquellas que nacimos entre
los años 60s y los 90s, una serie de instituciones como los medios de
comunicación, la industria del entretenimiento, instituciones
científicas e incluso muchas veces el mismo estado, nos vendieron una
imagen del futuro adornada con campos de fuerza, colonias en Marte,
patinetas antigravedad, teletransportación y estaciones espaciales. Sin
embargo, parece que nada de eso se materializó con la llegada las
tecnologías de la comunicación y las computadoras. Incluso muchos de los
avances en tecnologías de la comunicación que se esperaba que existieran
en este momento nunca se concretaron. Lo que sí tenemos, por ejemplo,
son computadoras analizando datos financieros para generar predicciones
que orientan a los inversores para aumentar la velocidad en la toma de
decisiones y la competitividad de ciertas empresas en el mercado de
capitales. Es decir que incluso en lo que se refiere a nuestros sueños
más mojados con el desarrollo de la informática y sus aplicaciones, la
innovación y lo que se nos presentó como la realización de muchas de
nuestras expectativas, terminaron atendiendo más a la forma en que
funciona el capitalismo que a problemas concretos de la gente común, ni
hablar de la utopía de la abundancia y el fin del trabajo con la que
soñaban anarquistas y comunistas por igual.

Entonces un punto fuerte que Graeber pone en juego en este ensayo es que
las instituciones que promovían estos pronósticos tipo fantasía sobre el
futuro, no eran dos o tres loquitos, sino gente que sabía lo que hacía y
que comunicaba desde cierta autoridad y cierta experticia para enunciar
la dirección hacia donde iban las cosas. Este mito contaba con el
respaldo de la memoria cultural del progreso y las innovaciones
científicas y tecnológicas que tuvieron lugar entre el 1900 y la década
del '20, para marcar una aceleración posterior. Por eso, al no
concretarse esas predicciones, nos quedamos preguntando qué pasó.

Entre los '50 y los '60 se comienza a dar una caída en la innovación
científica y tecnológica, con las microondas, las pastillas
anticonceptivas y los lásers como los desarrollos más sobresalientes de
la época. De ahí en más el desarrollo tecnológico se convirtió en una
recombinación de tecnologías ya existentes o en la comercialización
masiva de cosas que ya se habían inventado y habían estado esperando la
concreción de ciertas condiciones de producción para poder impactar en
el mercado. Es loco que hayan habido cambios fundamentales en los
paradigmas científicos en los últimos cuarenta años, mientras que si se
mira de cerca el enlentecimiento del desarrollo tecnológico va de la
mano de una disminución en la producción de conocimiento científico.

Que la ciencia y la técnica están relacionadas estrechamente es casi una
obviedad pero lo que marca Graeber es la existencia de cierto horizonte
imaginativo acotado para re-crear el presente. El peligro de retener
esta idea es que nos puede llevar a pensar que nada está pasando en los
campos de la ciencia y técnica, cuando en realidad pasan cosas que
moldean el futuro en cierta dirección. En realidad, dice Graeber, la
disminución en la cantidad de _papers_ científicos publicados y la caída
del desarrollo tecnológico a partir de los '60 fueron invisibilizados
por la épica publicitaria de la carrera espacial.

Es irónico que detrás de la tensión que se mantenía entre la unión
soviética y el occidente capitalista, se perpetuaba un relato de
prosperidad en la apariencia de la aceleración del progreso del
conocimiento. Esa apariencia montó una pantalla de propaganda sobre el
hecho de que las economías de escala que llenaban los mercados de bienes
de consumo se contraponían a las políticas de la clase trabajadora.
Esta estrategia de promover el consumo se instaló después de la segunda
guerra mundial, donde los gobiernos de los países capitalistas se
metieron de lleno en la campaña de crear políticas laborales para
combatir la amenaza de la revolución y los movimientos de izquierda.

Terminada la carrera espacial, el capitalismo se replegó a formas de
desarrollo tecnológico alineadas con modelos decentralizados de
producción para poner en oferta una masividad de productos que
respondieran a los estándares de consumo del libre mercado. En ese
contexto, los grandes proyectos de los gobiernos no desaparecieron, sino
que se trasladaron a la investigación militar, armas, comunicaciones,
monitoreo y seguridad nacional. Este cambio explica por qué no tenemos
fábricas robot: porque las investigaciones se dirigieron hacia otro
lado. Entonces en lugar de tener robots produciendo cosas, tenemos
robots ejecutando operaciones de combate.  Lo que se automatiza no es la
producción de abundancia, sino la producción de sujetas disciplinadas,
mutiladas o asesinadas.

Un procedimiento que habilita el texto es considerar a la computadora
personal y a las herramientes computacionales de análisis de datos
masivos como dispositivos que vinieron a cerrarle el callejón a todas
esas promesas sobre el futuro, relegándonos al conformismo de
maravillarnos con las interfaces de los medios sociales de apropiación
de trabajo no remunerado y a sobrevalorar los indicios de las
innovaciones en telecomunicaciones.

Mandel, un autor que Graeber referencia bastante, situaba una tercera
revolución industrial para el momento de las computadoras personales,
luego de la cual se alcanzaría un nivel de automatización tal que se
terminaría el trabajo como se viene dando desde los últimos tres siglos.
Estos cambios significaban la posibilidad del fin del trabajo y
generaron, en las pensadoras de la época, una serie de entusiasmos y
debates sobre el futuro de la clase trabajadora. Pero de nuevo, la
tercera revolución industrial nunca pasó. Lo que sí pasó fue que dichos
avances combinados con nuevas formas de transportar mercancías se
impusieron como el modelo que emergió de la crisis del petroleo y como
resultado, la parte medioambientalmente más jodida de la industria migró
a los sures globales. Esto les permitió a las grandes transnacionales
contratar mano de obra barata para (irónicamente) operar procesos de
producción técnicamente poco sofisticados y así mantener ciertos
estándares de ganancia. Junto con esta tendencia, Graeber identifica la
aparición de otras formas de trabajo en el norte global, que
reconfiguraron a las trabajadoras para la prestación de servicios y/o
para la producción masiva de datos o conocimiento y el consumo de
ansiolíticos.

En el caso paradigmático de China, últimamente se ha venido dando un
proceso muy acelerado de tecnificación y automatización, que si bien
ocurre en un contexto y en unas condiciones muy diferentes a las que se
pueden dar en otras regiones, abre la duda sobre qué viene después de la
tecnificación de las industrias geopolíticamente situadas en el tercer
mundo.  ¿A dónde van a migrar las estructuras del modelo productivo
cuando ya no haya una periferia donde producir sea tan rentable?  De
nuevo, qué va a pasar con el futuro...  Acá Graeber prefiere dar un giro
para otro lado y retoma la cuestión sobre por qué las predicciones del
futuro que había en el pasado no se concretaron en el presente.

Entonces identifica dos posibilidades: o que aquellas predicciones hayan
sido irreales --en cuyo caso deberíamos cuestionarnos porqué tanta gente
(supuestamente) tan capa estaba equivocada--, o que de hecho, las
predicciones eran realistas --en cuyo caso deberíamos pensar qué fue lo
que bloqueó ese progreso. Sin embargo, la explicación clásica es que las
expectativas sobre el futuro eran poco realistas y que la competencia
entre eeuu y la unión soviética daba la impresión de que estaba
sucediendo un progreso mucho más grande del que efectivamente estaba
sucediendo. De hecho, toda la era espacial se trató de estas dos
autoproclamadas sociedades pioneras contando su versión del mito de la
expansión ilimitada. ¿Cómo no iba a haber confusión sobre cómo sería el
futuro?  El relato que se le contaba a una nena en el 1900 sobre cómo
sería el futuro, resultó ser muy acertado a cómo realmente fueron los
años '60, había submarinos, radares, televisiones y cohetes; mientras
que lo que se le contaba a una nena en los '60 nunca resultó
concretarse. Acá la explicación clásica se queda corta.

Graeber nos propone entonces que veamos que toda la cuestión del relato
sobre el futuro fue diseñada por los intereses de las clases dominantes
en reacción a una realidad tecnológica que por un momento pareció
cuestionar la perpetuación de su poder. Un detalle a tener en cuenta es
que no había nada emancipatorio en las tecnologías emergentes de esa
época, sino que a duras penas (que no es decir poco), habilitaban la
posibilidad de una batería de fantasías de emancipación a futuro. Fue
una época en la que toda propuesta de transformación era diseccionada
por los intereses de los capitalistas en el marco de la lucha contra el
comunismo.

Como consecuencia del optimismo tecnológico de la época y bajo la
ubicuidad del determinismo tecnológico que tomaba lugar en las fantasías
de todas las ideologías, los capitalistas reaccionaron creando
dispositivos burocráticos para redireccionar y bloquear los desarrollos.
Esto configuró todo el sistema productivo, el mercado del trabajo, la
forma en la que se estudia, la forma en la que se investiga y se
producen avances tecnológicos hoy en día. Así, los capitalistas, al
haber identificado el poder revolucionario de la tecnología para
transformar la sociedad, impulsaron el libre mercado de la era de la
información para combatir los enfoques materialistas. De esta manera, el
surgimiento da la computadora personal y la tecnificación de la
producción consistieron en hitos en el desplazamiento del foco hacia una
narración no materialista de la historia.

Se trata de un debate sobre el trabajo inmaterial o mejor dicho, trabajo
material versus trabajo inmaterial, es una discusión que cobró vigencia
con el surgimiento de la sociedad de la información. Las teorías que
abren la era de la información hacen una propuesta de liberación de la
materia y al mismo tiempo mantienen una relación cercana con el complejo
industrial-militar. En la práctica, la implementación de estos modelos
de sociedad del conocimiento fueron financiados por contratistas
militares. Entonces la principal estrategia de marketing de estas
propuestas de invisibilizar el origen de la procedencia de los recursos
financieros no pudo terminar de desplazar a la materia de la centralidad
que ocupaba en otros modelos de sociedad.

Según Graeber la cuestión material sigue teniendo vigencia en la
problemática porque los capitalistas nunca invirtieron en robots para el
parque industrial en el que convirtieron el tercer mundo, porque dicha
inversión, al eliminar a la trabajadora, al trabajo y por lo tanto, al
valor, hubiera disminuido la plusvalía sobre la cual se sostenían sus
ganancias. Por eso las inversiones destinadas al desarrollo de cohetes y
robots fueron redirigidas a tecnologías de la información bajo una serie
de condiciones que permitieron un manejo del impacto de estas sobre el
orden existente y su integración en una visión que no amenazaba las
condiciones de acumulación del capital en el futuro. Por su parte los
futurólogos se encargaron de crear el marketing de estas nuevas
tecnologias --al convencernos de que las computadoras pueden ser parte
de nuestra vida cotidiana, por ejemplo-- sobre la base de otra fantasía,
aquella en la que se puede generar una sociedad estable basada de su
capacidad de reproducir las condiciones que la hacen posible.

Entonces cuando nos preguntamos qué pasó con los autos voladores, con
las fábricas robot y con las fantasías que teníamos de niñas, la
respuesta que nos ofrece Graeber es que no fueron financiadas y en su
lugar obtuvimos tecnologías de control social y sometimiento de la clase
trabajadora. Esta propuesta de Graeber se contrapone a la interpretación
habitual del neoliberalismo respecto de su prioridad en lo económico
sobre lo político y señala que todos estos hechos que revisa en el
ensayo, son una evidenca de cómo en el sistema neoliberal, lo económico
es secundario. Con la llegada de esta etapa del capitalismo se pierde la
posibilidad de tener una tecnología que traiga nuestros sueños más
descabellados a la vida, como decir, los autos voladores.
