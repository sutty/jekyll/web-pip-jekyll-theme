---
layout: actividad
wppost_name: dia-de-aaron-swartz
title: Día de Aaron Swartz - 9/11 en La Plata
date: 2019-11-02
tags:
  - evento
  - copiona
  - copyfight
  - pirateria
  - talleres
image:  
  path: public/images/aaron1.jpg
description: |
  El 9/11 se conmemora en todo el mundo el día de Aaron Swartz,
  programador y activista autor de "El manifiesto por la guerrilla del
  acceso abierto".
---

![Día de Aaron Swartz - Flyer 2](/images/aaron2.jpg)


El 9/11 se conmemora en todo el mundo el [día de Aaron
Swartz](https://aaronswartzday.partidopirata.com.ar/), programador y
activista autor de "[El manifiesto por la guerrilla del acceso
abierto](https://endefensadelsl.org/guerrilla_del_acceso_abierto.html)".

El Partido Interdimensional Pirata junto a [Osmiornica
biblioteca](https://www.instagram.com/osmiornicabiblioteca/) y
[Laberinto Casa Club](https://www.instagram.com/laberintocasaclub/)
organizó para esta fecha una serie de talleres, zona de hacklab autónoma
y feria de zines.

¡Las actividades son libres y gratuitas!

[/Nos guiamos x estos códigos para
compartir/](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html)

Cocina de laberinto abierta, comida vegana, dulces y bebidas.

¡¡Les esperamos!!

**Recibimos baterías de notebook y celular para reciclar.**

.Seremos puntuales.

☠️🐙🐇

Casa Laberinto queda en La Plata 9/11 de 14:30 a 20 hs. en 13 #1293
entre 58 y 59

Actividades:

* Feria de Zines y Utopías Piratas
* Taller: sitios web P2P con dat y Beaker
* Taller: armado de bibliobox
* Taller: armado de baterías portátiles recicladas
* Cómo piratear
* Apostasía de redes de vigilancia
* Sitios seguros con Sutty
* Presentación de Zine + Libro de Utopía Pirata

Trae tu pendrive, habrá copiona!

Estamos organizándonos para ir y volver desde CABA en este grupo de
Telegram: <https://t.me/BarcaPortenaConDestinoLaPlata>
