---
title: partido interdimensional pirata
description: 'Somos un colectivo de personas y cyborgs que nos organizamos en base
  a relaciones de afinidad, solidaridad, cuidados mutuos y afectividad. Esto no significa
  que tengamos que estar de acuerdo en todo ni que pretendamos homogeneizarnos en
  algún “estilo pirata”. Significa, sobre todo que desechamos las lógicas instrumentalistas
  y productivistas. Por sobre “los fines” o “los productos” de ciertas acciones u
  objetivos políticos preferimos ejercitar otras lógicas: de autocuidados, procesuales,
  de acompañamiento, de relacionamiento.'
image:
  path: public/pip.jpg
  description: ''
social:
- telegram
- mastodon
- twitter
- mail
sub_dominios:
- https://aaronswartzday.partidopirata.com.ar/
- https://hacklab.autonomous.zone/
- https://lumi.partidopirata.com.ar/
- https://desobediencia.partidopirata.com.ar/
- https://dpublico.partidopirata.com.ar/
- https://ñufla.partidopirata.com.ar/
- https://zine.partidopirata.com.ar/
- https://archivo-pirata-antifascista.partidopirata.com.ar/
permalink: colectivo/
draft: false
order: 1
layout: colectivo
uuid: 4dd12546-0870-4489-a8da-51b2c036e385
render_with_liquid: false
usuaries:
- 27
created_at: 2021-05-09 00:00:00.000000000 +00:00
last_modified_at: 2023-12-02 15:15:07.891528498 +00:00
---

<h1>PIP | partido interdimensional pirata</h1><p>Somos un colectivo de personas y cyborgs que nos organizamos en base a relaciones de afinidad, solidaridad, cuidados mutuos y afectividad. Esto no significa que tengamos que estar de acuerdo en todo ni que pretendamos homogeneizarnos en algún “estilo pirata”. Significa, sobre todo que desechamos las lógicas instrumentalistas y productivistas. Por sobre “los fines” o “los productos” de ciertas acciones u objetivos políticos preferimos ejercitar otras lógicas: de autocuidados, procesuales, de acompañamiento, de relacionamiento.</p><h2>Qué hacemos</h2><ul>
<li>Software y hardware libres (<a href="https://partidopirata.com.ar/2019/11/02/declaracion-de-liberacion-del-software-libre/" rel="noopener" target="_blank">¡y antipatriarcal!</a>)</li>
<li>(re)apropiación crítica de las técnicas de biopolíticas</li>
<li>Movimientos de base o territoriales de justicia social</li>
<li>Prácticas de cuidados antiopresivos</li>
<li>Desobediencia cibernética</li>
<li>Prácticas de cuidados digitales y autodefensa digital</li>
<li>Transhacktivismos y hacktivismos LGTTTBIQA*</li>
<li>Comunicación alternativa, comunitaria, popular</li>
<li>Derecho a la comunicación</li>
<li>Datos abiertos, democracia participativa</li>
<li>Autodeterminación &amp; #DoItOurselves</li>
<li>Prácticas de cuidados mutuos y colectivos</li>
<li>Artes digitales</li>
<li>Cultura libre</li>
<li>Editoriales libres, independientes y autogestivas</li>
<li><a href="https://es.wikipedia.org/wiki/Sousveillance" rel="noopener" target="_blank">Sousveillance</a></li>
<li>Estrategias de resistencia</li>
<li>Ecosocialismo, permacultura y <em>low techs</em>
</li>
<li>Emancipación tecnológica y cognitiva</li>
<li>Economías populares y solidarias</li>
</ul>
<h2>Cómo nos organizamos</h2><p>En el Partido Interdimensional Pirata tomamos decisiones por consenso y nos
organizamos alrededor de barcas, que son espacios auto-organizados alrededor de
temas, problemáticas, acciones, afinidades y proyectos.</p><h2>Cómo se toman decisiones</h2><p>Les piratas tomamos decisiones por <strong>consenso</strong>. Esto quiere decir que
presentamos nuestras propuestas, las discutimos entre todes y llegamos a una
posición común. No quiere decir que todes tenemos que votar de la misma forma,
sino que al tomar una decisión estamos teniendo en cuenta las posiciones de
todes les piratas involucrades. El consenso es un proceso constante de una
acción, lo que nos permite actuar con flexibilidad, darnos cuenta de nuestros
errores y aciertos y considerar otras posiciones.</p><p>Usamos las <a href="https://utopia.partidopirata.com.ar/democracia_directa.html" rel="noopener" target="_blank">"Herramientas para la democracia directa"</a> como guía
y nuestra propia plataforma de toma de decisiones por consenso:
<a href="https://lumi.partidopirata.com.ar" rel="noopener" target="_blank">Lumi</a>.</p><h2>Barcas</h2><p>Las barcas son grupos de afinidad sobre temas específicos relacionados con
nuestros activismos. Para crear una barca solo es necesario el consenso y la
participación de tres piratas. Las barcas tienen autonomía para actuar pero
tienen el compromiso de funcionar de acuerdo a este documento y sostener los
códigos para compartir internamente y hacia las actividades que organicen
(talleres, encuentros presenciales, etc.). Es decir, deben reconocerse y ser
reconocidas como barcas piratas P)</p><p>Las barcas se comunican entre sí y colaboran en distintas acciones. Para no
pisarnos, pedimos colaboración de otras barcas cuando hacemos cosas que se
solapan.</p><p>Algunas barcas de las que podés participar son las siguientes... No son todas,
son las que están abiertas a través de chats grupales en Telegram y son públicas
porque cualquier interesade participa de sus encuentros, producción y
hacktividades.</p><ul>
<li>
Utopías Piratas: Edición de libros y zines (pedir invitación).
</li>
<li>
Barca Transfeminista
</li>
<li>
<a>Barca LGBTTTIQA</a>
</li>
<li>
<a href="https://t.me/joinchat/DQyUbE_uo_NhJQ1F2vLbBw" rel="noopener" target="_blank">Varones Antipatriarcales</a>:
Debate sobre deconstrucción cis-masculina y masculinidades, antipatriarcal!
</li>
<li>
<a href="https://t.me/partido_pirata_la_plata" rel="noopener" target="_blank">Barca Platense</a>: Piratas oriundes de la
ciudad de La Plata.
</li>
<li>
<a href="https://t.me/joinchat/BayXVRTDVFJpKqZdli-yiQ" rel="noopener" target="_blank">LUMI</a>: Donde estamos
desarrollando nuestra plataforma digital de toma de decisiones.
</li>
<li>
Interdimensional: Punto de contacto para piratas perdidas en el espacio (pedir
invitación).
</li>
<li>
Barca Grog &amp; Tor (autodefensa digital): Pedir indicaciones P)
</li>
<li>
Barca Comunicación Pirata
</li>
<li>
Barca Astillero de memes
</li>
<li>
Barca Infra
</li>
<li>
Barca P2P
</li>
<li>
Barca Wiki
</li>
<li>
<a href="https://t.me/BarcaSindicalPirata" rel="noopener" target="_blank">Barca Sindical Pirata</a>: Sobre temas relacionados al mundo del trabajo. Bajo dependencia o independiente. (No quedan excluides les autogestionades).
</li>
<li>
<a href="https://t.me/PipGalponeoTecnico" rel="noopener" target="_blank">Galponeo Técnico</a>: Aca podés preguntar, aprender y compartir sobre temas más específicamente técnicos.
</li>
<li>
Barca PyData: La barca es de análisis de datos. Son bienvenidos todo tipo de saberes y los conocimientos de otros lenguajes de programación. (Pedir invitación)
</li>
<li>
<a href="https://t.me/joinchat/Uy_cirGRLwPUq8ao" rel="noopener" target="_blank">Barca de cine</a>.
</li>
</ul><h2>Difusión</h2><ul>
<li>
<a href="https://t.me/pipar_vesperino" rel="noopener" target="_blank">Vespertino Pirata</a>: Novedades del PIP en forma
de mensajes unidireccionales, estos además se redireccionan para ser
comentados y debatidos en el canal público.
</li>
<li>
<a href="https://t.me/PartidoInterdimensionalPirata/" rel="noopener" target="_blank">Canal público</a>: Ahí podrás
saludar, esperar un rato y conversar con nosotres y, por sobre todo, con
muches otres cyborgs. Te pedimos que no te presentes con nombre y apellido,
dónde y cómo vivís, etc. Sino con un alias/nombre de usuarie que te
identifique. Por cuestiones de cuidados personales y colectivos, queremos
tener precaución respecto de la cantidad de información proporcionada de
manera pública que pueda volvernos identificables, individualizables. Este
canal es moderado a pulmón con nuestros esfuerzos colectivos, se baneará a
discreción. Si te sentís incomode y querés advertir de comportamientos y
comentarios, avisanos a nuestra identidad colectiva
<a href="https://t.me/ReneMontes_bot" rel="noopener" target="_blank">René Montes</a>.
</li>
<li>
<a href="https://t.me/memazoreloaded" rel="noopener" target="_blank">Memazo</a>: es como ver el cable interdimensional.
</li>
<li>
<a href="https://utopia.partidopirata.com.ar/zines/apostatemos_de_las_redes_asociales.html" rel="noopener" target="_blank">Fediverso</a>:
Encontranos en el fediverso en <a href="https://todon.nl/@pip" rel="noopener" target="_blank">https://todon.nl/@pip</a>
</li>
</ul>