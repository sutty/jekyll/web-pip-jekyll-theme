---
layout: post
wppost_name: a-nosotres-no-nos-cuidan-las-apps
title: A nosotres no nos cuidan las apps, nos cuidan nuestras alianzas político - afectivas
date: 2020-05-16
tags:
- declaración
- covid
- cuarentena
description: |
  Tras el anuncio presidencial de la obligatoriedad del uso de la
  aplicación CuidAr, problematizamos sobre dicha medida tecnocrática
  y lo que representa en el contexto del capitalismo cognitivo. ¿Por qué
  el Estado toma esa medida? ¿Cuál es el uso y cuidado que se le dará
  a la información de les trabajadorxs? No sabemos
---


## Medios y tecnologías autoritarias para una finalidad autoritaria

En estos días se anunció lo que intuíamos iba a ser el desarrollo de las
medidas bajadas desde los estados para controlarnos con la excusa de la
pandemia. Nos enchufaron una app a la que podemos hacer mil críticas
técnicas, pero lo importante es la app como medida tecnocrática y lo que
representa en el contexto del capitalismo cognitivo.

Según los medios hegemónicos la app que acaban de anunciar de forma poco
clara y en un contexto de limbo de derechos civiles, solo se trata de
una encuesta a completar cada 48 horas para poder renovar el permiso de
circulación. Sin embargo, para eso los datos tienen que ir a guardarse
algún lado. En este caso van hacia los servidores de _Amazon_, la misma
que obliga a sus trabajadorxs a seguir trabajando como si la situación
sanitaria no existiera (para elles la cuarentena nunca fue una
posibilidad). Además, _Amazon_ es el proveedor más grande de
infraestructura de Internet de forma que la mayoría de los servicios que
usamos cotidianamente están alojados allí. Así, es muy probable que en
concreto, los datos que genere la app se alojen físicamente en centros
de datos de _Amazon_ en Brasil, pero que también se distribuyan por todo
el mundo, a todos los otros miles de centros de datos que tiene la
corporación. Al fin y al cabo no son solo explotadores de personas
y pioneros en control cibernético, sino también explotadores de
computadoras y datos.

## Articular lo político para hacer posibles los reclamos técnicamente imposibles

Cuando como colectiva nos pusimos a intercambiar ideas sobre qué
características debería cumplir la app, inmediatamente pensamos en que
sea auditable, que su código fuente sea público para saber qué se hace
con nuestros datos, que se generen y comuniquen políticas específicas de
protección de datos y la privacidad de les usuaries, debían ranquear la
lista. Pero que una corporación sea dueña de los servidores donde se
alojan físicamente los datos de la app representa en sí un problema en
términos de soberanía de datos, uso y protección tal, que barre por
completo con la posibilidad de que todos esos reclamos se cumplan. Es
decir, claramente hay distintos órdenes de reclamos que hace falta
enunciar y problematizar en lo que repecta a _CuidAr_. En particular ya
simplemente el hecho de que desde su propuesta no se plantee como una
herramienta articulada a un proyecto epidemiológico y sanitario concreto
(testeo masivo, por ejemplo) seguramente recalca la necesidad de que
dichos reclamos no sean solamente de caracter técnico.

Por otro lado, todavía tenemos dudas respecto de la viabilidad legal de
la "imposición", dado que el presidente comunicó en su conferencia de
prensa del 8/5 que el uso de la app sería obligatorio para poder
circular en caba y el amba. Lo que nos parece importante notar es que
seguramente en la experiencia más cotidiana de les usuaries, de una
u otra forma se va a prestar como chivo expiatorio para la brutalidad
y el exceso policial y esto tiene que ver con que la realidad del
impacto de cualquier dispositivo, ya sea técnico o jurídico, no está
prescripto ni en el código fuente, ni en la letra de la ley.

Es un hecho que las apps que nos siguen y escuchan lo que deseamos
constante y cotidianamente ya existían.  Son las que generan las
ganancias multimillonarias de los ricos que ahora más que nunca sueñan
con una profundización en la conquista y extracción de valor de nuestros
espacios más íntimos.  Son los que han montado la infraestructura del
control que se prestó tan bien a sus intereses durante la cuarentena:
trabajo ultra-precarizado alrededor del transporte y la logística
a domicilio, el _software_ de copiar y pegar para armar en dos meses
apps de seguimiento, centros de datos distribuidos por todo el mundo,
etc. Y son apps que tienen nombre y apellido,  son _Uber_, _Rappi_,
_Glovo_, _Amazon_, _Microsoft_, _Google_, _Twitter_ y _Facebook_. Pero
el hecho de que en este caso sea el estado el que obliga a usar una
apliación que acumulará info de la población que la use, seguramente
merezca un análisis aparte que ponga en diálogo la medida en cuestión
con otras tecno-políticas similares como pueden ser el reconocimiento
facial y el voto electrónico. [Habiendo protocolos de _contact tracing_
más
seguros](https://utopia.partidopirata.com.ar/zines/la_vida_y_la_libertad.html),
que respetan nuestro derecho al anonimato y la privacidad, nos parece
que nuevamente lo técnico se plantea como una gestión silenciosa, ya que
el tiempo de almacenamiento de los datos y la finalidad con la que se
usarán --por dar un ejemplo de dos cuestiones que podrían ser críticas--
son cosas que permanecen en total oscuridad y no están garantizadas de
hecho.

## Hay que tener código para debatir

El código que estuvo rondando por ahí indica que la app fue hecha a las
apuradas y bajo la lógica del mínimo producto viable.  Pero el punto no
es ser carnerxs del trabajo de otres trabajadorxs como nosotres, porque
en definitiva, la calidad de un código es algo que mejora con el tiempo.
El problema, además de ser el mismo que nos individualiza para gestionar
nuestra salud y fuerza productiva, y ser el que nos hace llegar al
fetiche técnico de despellejar a le compañere, es el problema de nuestro
caracter de desposeides, que la app se baje desde arriba, que no sea
"transparente" a les usuaries respecto a las políticas técnicas que
tienen que ver con donde se va a alojar y para qué se va a usar la data,
tampoco es pedir que haya tiempo y financiamiento para ese tipo de
herramientas; el problema es que haya tiempo y guita para este tipo de
apps porque es el tipo de herramientas oficiales que se quieren
desarrollar. Les piratas queremos que haya tiempo y financiamiento, pero
para que podamos decidir cómo queremos que sean estas herramientas
tecnológicas.

Queremos decir que esta y otras apps son parte de un entramado
capitalista y civilizatorio donde se meten la urgencia y la hegemonía
tecnocrática.  Un proceso que venía avanzando con el gobierno "basado en
datos", con control cibernético de las personas, ahora se acelera de
prepo. Y el control es lo que ofrecen como posible las tecnologías que
desarrollan las empresas que recurren a los servicios de las
corporaciones.  Quizás es por esa [voluntad de controlar que llevan
dentro](https://desobediencia.partidopirata.com.ar), que se generan las
posibilidades técnicas.

## Clasismo y tecnocracia como la frutilla del postre

Por supuesto, en el recorte que hace el paradigma tecnócrata, quedan por
fuera quienes no tienen acceso a los dispositivos que median nuestra
relación con el poder.  Quizás el punto es que ya ni median, sino que
cada vez más solamente botonean. Porque el sueño del control de tener un
policía en cada ciudadane es el mismo que el de tener un policía en el
bolsillo de cada ciudadane [_¿Y por qué no tener los
dos?_](https://www.anred.org/2020/05/07/el-nuevo-poder-disciplinario-en-jujuy-control-entre-ciudadanos-y-big-data-para-todos/).

Como Partido Interdimensional Pirata venimos llevando a cabo
experiencias que sitúan las tecnologías en el territorio de las
necesidades, condiciones y contextos de quienes las usamos. Sabemos que
no hay tecnologías neutras y que los márgenes de apropiación
y posibilidad de autodeterminación tecnológica deben ser permanentemente
cuestionados.   Por eso, lo que nos preguntamos es, cuando la app sea
definitivamente obligatoria (no está claro aún), ¿Quiénes la van a tener
que usar?  ¿Dónde van a funcionar?  Porque lo que termina profundizando
la tecnocracia es la estructura de clases y la distribución desogual de
los privilegios.  Para participar del gobierno cibernético hay que tener
dispositivo.  También [conexión
a Internet](https://desobediencia.partidopirata.com.ar/argentina/enacom/).
[Y
electricidad](https://www.anred.org/2020/05/08/con-fuerte-rechazo-larreta-obtuvo-una-ley-de-emergencia-que-garantiza-ajuste/)
para recargar las baterías.  Ni hablar del agua potable y otros
servicios básicos para la vida.  

Lo que queremos problematizar como colectiva pirata es que cuando estas
tecnologías de seguimiento y control son desarrolladas y puestas en
circulación rápidamente, apelando a la urgencia de la necesidad y además
de eso, se plantean como las únicas tecnologías posibles; se hace
coartando la posibilidad de activar y desarrollar otras redes
tecnológicas de sorroridad y ayuda mutua que nos empoderan y promueven
la autonomía y los cuidados colectivos.
