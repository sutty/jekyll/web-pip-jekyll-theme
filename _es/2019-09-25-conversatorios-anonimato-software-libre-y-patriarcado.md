---
layout: post
wppost_name: conversatorios-anonimato-software-libre-y-patriarcado
title: Conversatorio sobre Anonimato, Software Libre y Patriarcado
date: 2019-09-25
tags:
  - anonimato
  - privacidad
  - transfeminismos
  - patriarcado
  - conversatorio
image:  
  path: public/images/conversatorio.png
description: "Lo personal es político.  Lo técnico es político.  ¿Qué pasa cuando las herramientas y estrategias que activamos sirven para apañar machirulos, proteger trolls y abusadores y sostener el cistema que nos violenta?"
---


**Quiénes:** Partido Interdimensional Pirata, Hacklab Violeta, Tierra
Violeta, Red Argentina de Género, Ciencia y Tecnología

**Lugar:** Tierra Violeta, Tacuarí 538 (CABA) (Espacio accesible para
usuaries de sillas de ruedas)

**Primer Encuentro:** martes 1/10 - 18 horas

**Segundo Encuentro:** martes 5/11!

**Hablan:** Quienes quieran :) ¡Es un conversatorio!

**Compartimos:**

Lo personal es político.  Lo técnico es político.  ¿Qué pasa cuando las
herramientas y estrategias que activamos sirven para apañar machirulos,
proteger trolls y abusadores y sostener el cistema que nos violenta?
Genealogías hackers en el Software Libre: "nuestro ídolo es un forro"
(¿?) ¿Y si todas esas herramientas estaban pensadas desde el principio
para sostener abusadores? ¿Podemos quedarnos tranquiles diciendo que las
herramientas son neutras y dependen de la mano que las use?  ¿Nos siguen
sirviendo las mismas herramientas y estrategias o necesitamos
revisarlas, inventar unas propias?

Nos juntamos a problematizarnos los hacktivismos frente y en contra de
los abusos, la trata, el abuso sexual infantil y desde los
hacktransfeminismos, pensando estrategias y herramientas existentes y
por venir P)


## RMS / Epstein / MIT

<https://medium.com/lasdesistemas/el-genio-machista-tan-genio-no-es-d04ee4057dce>
<https://write.as/pazpena/hay-un-elefante-que-recorre-silicon-valley>

## Códigos para compartir

<https://wiki.partidopirata.com.ar/c%C3%B3digos-para-compartir/>

## Licencias no violentas

<https://thufie.lain.haus/NPL.html>
<https://firstdonoharm.dev/>
<https://github.com/Laurelai/anti-fascist-mit-license>
<https://t.me/lovelyanarchistmemes/1533>
