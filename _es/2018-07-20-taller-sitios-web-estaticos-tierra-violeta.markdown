---
layout: actividad
title: ¡Taller de sitios web estáticos en el CC Tierra Violeta!
date: 2018-07-20
wppost_name: taller-sitios-web-estaticos-tierra-violeta
tags:
  - utopías piratas
  - taller
  - tierra violeta
image:  
  path: public/images/taller_de_jekyll.png
description: "Todos los sábados de agosto y septiembre de 14 a 16 hs. en el CC Tierra Violeta, Tacuarí 538 (CABA).  Escribinos a contacto@partidopirata.com.ar para confirmar participación."
---

**Todos los sábados de agosto y septiembre de 14 a 16 hs. en el CC
Tierra Violeta, Tacuarí 538 (CABA).  Escribinos a
contacto@partidopirata.com.ar para confirmar participación.**

_Tener un sitio web en los últimos años dependió de la infraestructura,
los términos y condiciones y las políticas de privacidad de terceros.
Las plataformas para publicar nuestros sitios web ya no dejan lugar a la
imaginación y al deseo y con la web 2.0, se convirtieron en estériles y
estandarizadas interfaces, siempre con el riesgo de ser trolleadas,
censuradas y eliminadas..._

Esta podría ser la invitación a desarrollar tu primer sitio web (o
página en Internet). Pero también, si ya tenés experiencia haciendo
sitios o micrositios (como en blogspot, worpress, wix u otros...), puede
ser la propuesta para ganar independencia y seguridad en tus páginas,
deshaciéndote de viejas costumbres, de sitios que se llenan de virus o
que no respetan tu privacidad (iupi!)... Sea cual sea el caso, esta es
una invitación abierta a que participes con nosotres de un taller en el
que repensemos juntes la forma de concebir nuestro(s) espacio(s) en
Internet, de manera que podamos construir estrategias colectivas para
generar comunidades online libres, seguras, ecológicas, con criterios
trans-feministas y de cuidados, al tiempo que exploramos diferentes
opciones, tanto estéticas como de autogestión en el alojamiento (es
decir, donde "vivirá", efectivamente, nuestro sitio).

La tecnología principal con la que vamos a jugar :P para desarrollar
nuestros sitios se llama Jekyll. Vamos a alojarlo en un servidor casero
o en un servidor no mainstream de elección conjunta.


## ¿Conocimientos necesarios?

¡Vení así nomás!

## Requisitos

Mejor si traés una computadora que puedas usar para hacer las prácticas.
Mejor mejor si tiene GNU/Linux (Software Libre). ¡Pero de todas formas
podés venir! Escribinos antes, pero no hace falta que nos des datos
personales, solo queremos confirmar tu participación. Podemos asesorarte
si tenés consultas.

## ¿Tengo que inscribirme?

¡Sí! Escribinos antes así podemos organizar mejor el taller. **No es
necesario que nos dejes datos personales.**

Tené en cuenta que la participación de mujeres cis y trans, lesbianas,
travestis y transexuales, hombres trans, personas trans y no binarias
está becada por el CC Tierra Violeta y la RAGCyT.  La participación está
abierta a varones cis, aunque es arancelada para sostener el espacio.

## ¿Es apto para todo público?

El taller está pensado para todas las personas. Por el tipo de prácticas
que llevaremos a cabo, no pensamos en niñes, pero sí pueden venir
jovenes y adultes. Si bien no es necesario tener conocimientos previos,
cierta experiencia con el uso de una computadora sera necesaria para
poder seguir la propuesta. En cuanto a los géneros, no tenemos ningún
tipo de restricción; eso sí: tenemos un [código de
conducta](https://wiki.partidopirata.com.ar/C%C3%B3digos_para_compartir)
y buscaremos generar consensos para la convivencia. Sobre todo teniendo
en cuenta que estamos en un centro cultural f e m i n i s t a, no habrá
espacio para tolerar actitudes prepotentes y machistas.

## ¿¿Lo qué?? ¡FAQ!

* ¿Qué es un sitio web o página en Internet?
* ¿Entonces es un sitio o un micrositio?
* ¿Por qué ganaría independencia?
* ¿Por qué ganaría seguridad?
* ¿Por qué dicen que no voy a tener virus?
* ¿Por qué dicen que van a respetar mi privacidad?
* ¿Cómo vamos a construir estrategias colectivas? ¿Qué es eso?
* ¿Qué quieren decir con comunidades online libres, seguras, ecológicas,
  con criterios trans-feministas y de cuidados?
* ¿Qué opciones estéticas pretenden explorar?
* ¿A qué se refieren con autogestión en el alojamiento?

¡Vení al taller y enterate de todas las respuestas a todas tus preguntas!

## Gacetilla

### Taller de diseño y mantenimiento de sitios estáticos con Jekyll en Tierra Violeta

> En el marco del Taller Permanente de Informática y Género,
> Herramientas Informáticas y Digitales con Perspectiva de Género de la
> Red Argentina de Género, Ciencia y Tecnología (RAGCYT) junto con el
> Centro Cultural Tierra Violeta.

Todos los sábados de agosto y septiembre, de 14 a 16 horas, se realizará
un taller de armado y alojamiento de páginas web en el Centro Cultural
Feminista Tierra Violeta, en Tacuarí 538, Ciudad Autónoma de Buenos
Aires. Las personas que participen podrán desarrollar un sitio web
basado en el software libre Jekyll y otras herramientas de desarrollo
cooperativo.  Se trata de sitios web estáticos, seguros y sostenibles,
ya que una vez publicados no dependen de servicios especiales para su
funcionamiento. Les talleristas a cargo pertenecen a las Barcas
Transfeminista y Utopías Piratas del Partido Interdimensional Pirata.

Para participar, es necesario escribir previamente a
contacto@partidopirata.com.ar, sin enviar datos personales. No se
requieren conocimientos especiales, aunque es recomendable alguna
experiencia con el uso de la computadora. Si bien es preferible traer
una computadora, no es un requisito excluyente. La inscripción es libre
y gratuita para mujeres cis y trans, lesbianas, travestis y
transexuales, hombres trans, personas trans y no binarias.

En un esfuerzo por repensar las tecnologías y la capacidad de ser
artífices del propio destino digital, el taller propone aprovechar al
máximo los recursos disponibles desde un posicionamiento ecológico. A la
vez, al reflexionar sobre la autonomía, la soberanía tecnológica y la
autogestión, se cuestiona la dependencia de una infraestructura
corporativa, con términos y condiciones y unas políticas de uso y
privacidad que facilitan la censura y la extracción de datos.  En
cambio, se explorarán opciones que permitan la libertad en el
ciberespacio. En el taller se experimentará sobre algunas posibilidades
de alojamiento web autónomos, como el montaje de un servidor propio o
recurriendo a servidores justos y confiables.

En cuanto al diseño, se cuestionan las representaciones y modalidades de
hacer/acceder que proporcionan las interfaces estandarizadas en pos de
la creatividad.

Finalmente, si bien la convocatoria es abierta, la perspectiva
trans-feminista de les talleristas y del Centro Cultural Tierra Violeta
lo orientan a una convocatoria tradicionalmente excluida de otros cursos
y talleres de tecnología, aunque esto no sea así desde el mismo
nacimiento de la informática.  Es por esto que el taller es gratuito
para mujeres cis y trans, lesbianas, travestis y transexuales, hombres
trans, personas trans y no binarias.

El PIP tiene como valores fundamentales la libertad, la diversidad, la
pluralidad, la auto-organización, la horizontalidad y la igualdad. Por
ello, el taller es un espacio donde se busca que estos principios sean
respetados. Se promueve la cultura libre y espacios libres de machismos
y cis-sexismos. Las Barcas dentro del PIP son espacios autónomos de
creación, experimentación y acción.

Tierra Violeta es un espacio de acción y producción colectiva conformado
por la Biblioteca y Centro de Documentación Feminaria, el Centro de
Investigación y Formación Elvira López y el Centro Cultural y Teatro
Independiente. El Hacklab Violeta funciona allí desde hace dos años,
como un espacio de experimentación e intercambio en el que la ética
hacker se entrelaza con la feminista.
