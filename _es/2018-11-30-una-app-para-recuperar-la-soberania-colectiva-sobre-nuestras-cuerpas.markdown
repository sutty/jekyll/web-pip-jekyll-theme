---
layout: post
title: Una app para recuperar la soberanía colectiva sobre nuestras cuerpas
date: 2018-11-30
wppost_name: una-app-para-recuperar-la-soberania-colectiva-sobre-nuestras-cuerpas
tags:
  - represión
  - cuidados mutuos
  - movilización
description: "¿O cuantas veces terminamos fragmentadas por vernos obligadas a desconcentrar a las corridas? Con esta app proponemos que una de nuestras medidas más básicas de autodefensa sea conocer la ubicación de nuestras compañeras."
---

¿Cuantas veces nos pasó que nos detuvieron a una compañera y durante
unas horas no supimos dónde la habían trasladado?

¿O cuantas veces terminamos fragmentadas por vernos obligadas a desconcentrar a las corridas?

Con esta app proponemos que una de nuestras medidas más básicas de
autodefensa sea conocer la ubicación de nuestras compañeras.

En el marco estrategico de la autodefensa digital y el desarrollo de
herramientas que colectivamente identifiquemos necesarias y/o adaptables
a nuestros contextos organizativos, durante los últimos días venimos
probando una app que se llama µlogger que descubrimos, puede ser útil
dentro del conjunto de las medidas de autodefensa y cuidados mutuos que
todxs tomamos en marchas, movilizaciones, concentraciones, tomas,
coberturas, piquetes y otras medidas de acción directa. Esta app nos
permite conocer la ubicación y el recorrido de un grupo de celulares
Android, de forma tal que podemos saber la ubicación de nuestras
compañeras y centralizar información sobre su paradero en situaciones de
detenciones masivas, represión, corridas y/o desconcentraciones.

Acá abajo sistematizamos y compartimos los pasos para implementar el uso
de µlogger. Les invitamos a probarla con sus grupas y colectivas en la
marcha contra el G20 convocada para hoy.  Agradecemos sus devoluciones e
ideas para este u otros proyectos a <contacto@partidopirata.com.ar>.

El uso es anónimo y ad hoc, no registramos direcciones IP ni solicitamos
registro de cuentas con validación de identidad.  La app en sí solicita
usuarix y contraseña solo para que puedan distinguir quién es quién
dentro de su colectiva y nadie más pueda subir ubicaciones falsas.

La app µlogger es una app pre-existente, que reutilizamos para este
proyecto, que elegimos por ser liviana y prestar atención al uso de
batería, aunque originalmente está orientada a usos individuales.
Funciona enviando regularmente la posición geográfica obtenida del GPS a
un servidor propio y configurable que guarda nuestros recorridos.  Aquí
estaremos usando el del Partido Interdimensional Pirata, adaptado a usos
colectivos P)

Luego esta información se visualiza en un mapa con una dirección URL
especial solo conocida por el grupo, mostrando los recorridos de todxs
las personas.

## Ver el mapa de recorridos

* Ingresar a la dirección
  <https://partidopirata.com.ar/ulogger?grupo=lo_que_quieran>,
  modificando "lo_que_quieran" por el nombre del
  grupo/colectiva/organización o una frase al azar que elijan y
  compartan, preferiblemente sin espacios.  Esto se transmite de forma
  segura por Internet pero no se almacena de forma cifrada en el
  servidor (¡todavía!).  Cualquiera que tenga esta dirección puede ver
  los recorridos, con lo que uds. deciden cómo la quieren manejar.

* El mapa se actualiza automáticamente cada minuto, mostrando puntos y
  uniéndolos en un recorrido.

* Al tocar un punto, pueden ver el nombre de usuaria de la persona que
  lo marcó y la hora en que se tomó.

## Instalar y configurar µlogger y comenzar un grupo de recorridos

* Instalar la app µlogger desde
  [F-Droid](https://utopia.partidopirata.com.ar/zines/autodefensa_digital_para_activistes.html#f-droid)
  o en [descarga directa desde esta
  ubicación](https://f-droid.org/repo/net.fabiszewski.ulogger_208.apk)
  (pesa 1MB)

* Al abrirlo, usar el ícono del engranaje para configurarlo con:
  * **User:** lo que quieran que identifique a cada persona (nunca
    recomendamos usar nombre legal ni otras señas, pueden usar un
    pseudónimo/nick que tenga sentido dentro de su grupo)
  * **Pass:** lo que quieran y recuerden. Se transmite y almacena
    cifrada.
  * **Server:**
    <https://api.ulogger.partidopirata.com.ar/lo_que_quieran> la última
    parte de esta dirección tiene que corresponder el nombre de grupo
    que eligieron en el mapa de recorridos.
  * **Sincronización continua:** sí, esto envía las posiciones en tiempo
    real
  * **Cada cuánto tiempo:** lo que quieran, depende del uso de batería,
    puede ser cada minuto.
  * **Cada cuánta distancia:** 100m o lo que quieran.
  * **Inicio automático:** para que la app comience automáticamente
    aunque se reinicie el celular.  ¡Recuerden desactivarlo cuando ya no
    quieran registrar recorridos!
* Al volver, cuando dice "sincronizado" en verde en la pantalla
  principal es que se conectaron al servidor sin errores.
* Para enviar usan el botón "nuevo registro" con el nombre que quieran
  (la fecha de cobertura por ejemplo)
* Usar el botón "iniciar"
* El botón "subir" sube la información al servidor. Lo usamos solo si no
  activamos sincronización continua previamente.

## Posibles problemas

Como todo software, siempre puede haber imprevistos o funcionalidades
que no se adaptan a lo que necesitamos.  Identificamos que estas cosas
pueden pasar y nos interesa saber si les pasaron, si se resolvieron con
las indicaciones que pusimos o lo hicieron de otra forma, o si la
aplicación no funcionó para nada.  Además, si se les ocurren
funcionalidades nuevas.  Esto nos ayuda a mejorar la app y adaptarla al
uso de todxs P)

* Si se corta la conexión a Internet, tal vez haya que subir todos los
  puntos manualmente. Esto lo resolvemos apretando el botón "Subir"
  luego de "Inicio".

* Sale un cartel diciendo que no hay ningún proveedor de ubicación.
  Esto puede ser porque el GPS está bloqueado para todas las apps de
  Android.  Hay que ir a "Ajustes" o "Configuración" del celular (suele
  ser un ícono de un engranaje) y en la opción "Ubicación" activar el
  GPS.

* Si el ícono de sincronización da un error y queda en rojo, detener la
  sincronización y reiniciar la app.  Para reiniciar la app hay que
  abrir el listado de aplicaciones de Android (suele ser un botón
  cuadrado junto al de volver a la pantalla anterior) y cerrar la app
  usando la X o deslizándola hacia un costado.  Luego abrirla desde la
  lista de apps disponibles.  Esto hace que la app se vuelva a conectar
  al servidor.

* Para cerrar la aplicación y que no quede activa en segundo plano,
  debemos detener el trackeo y después cerrar la app desde el gestor de
  apps de Android.

