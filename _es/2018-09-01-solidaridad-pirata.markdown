---
layout: post
title:  Comunicado y llamado a la solidaridad pirata para este domingo
date: 2018-09-01
wppost_name: solidaridad-pirata
tags:
  - solidaridad
  - taller
  - infraestructura
description: "El pasado miércoles 29 de agosto, el Partido Interdimensional Pirata -y todas las piratas y aliadas- sufrimos un ataque al servidor en el que alojamos nuestros sitios y servicios internos que consistió en la eliminación de toda nuestra data, es decir nuestra memoria colectiva."
---

El pasado miércoles 29 de agosto, el Partido Interdimensional Pirata -y
todas las piratas y aliadas- sufrimos un ataque al servidor en el que
alojamos nuestros sitios y servicios internos que consistió en la
eliminación de toda nuestra data, es decir nuestra memoria colectiva.

No podemos aún precisar las condiciones ni los motivos que rodearon los
hechos. Sólo contamos con la información proporcionada por la empresa
dueña de los servidores: nos dijeron que la persona responsable de
administrar  el hosting (unx compañerx) habría hecho la confirmación
para la eliminación de la cuenta. Todavía aguardamos las fuentes que
podrían corroborarlo.

Si bien nos interesa llegar a conocer exactamente cuáles fueron las
acciones u omisiones que nos llevaron a esta situación, queremos poner
el énfasis en aquello que es más importante para nuestro movimiento.

Ante la gravedad y la tristeza de un hecho tan miserable, llamamos a la
conciencia pirata y les invitamos a colaborar solidariamente en la
jornada que convocamos este domingo (mañana) para reconstruir nuestra
infraestructura.

Para que todes les compañeres puedan participar, aun las que no tengan
los conocimientos técnicos, decidimos que dicha jornada sea a la vez un
espacio de (auto)formación, siguiendo la ética de camaradería pirata.

A la vez que nos ayudan con la reconstrucción, participarán en calidad
de asistentes a un taller digital de administración de sistemas P)

De esta manera, el alojamiento se realizará de forma distribuida: cada
pirata y compañere tendrá su propio servidor en el que albergará una
porción de infra pirata! (Aprendemos mientras construimos).

La cita es este domingo de 15 a 19 hs. (UTC -3) en el [canal público del
Partido Interdimensional Pirata en
Telegram](https://t.me/joinchat/Be5mbkLegzRdaJ-8Nf7XsA), si no están ahí
pueden escribir a <contacto@partidopirata.com.ar> o acceder al [canal
#ppar de IRC](https://webchat.pirateirc.net/#ppar) (el puente
telegram-irc no está funcionando).

En un servidor de emergencia ya tenemos mail, sitio, blog y el sitio de
la editorial utopía pirata han sido recuperados también temporalmente.

Finalmente, queremos reflexionar acerca de cómo un ataque de estas
características vulnera nuestra memoria colectiva y deja a muches
camaradas, compañeres, aliades, activistas y etcéteras sin buena parte
de los recursos para las luchas.

Gracias por la camaradería. P)
