# pip

Usamos haini.sh para generar un entorno reproducible en GNU/linux y ejecutar jekyll ahi adentro:

podés instalarlo con esta guía
https://0xacab.org/sutty/docs.sutty.nl/-/blob/no-masters/desarrollo/configurar-entorno.md#hainish

## Desarrollo 

Dentro del repositorio usamos go-task para compilar el sitio:

1. go-task bundle para instalar dependencias de jekyll

2. go-task pnpm para instalar dependencias de esbuild (js)

3. go-task build para construir el sitio y levantarlo en https://$sitio.sutty.local:4000

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
https://0xacab.org/sutty/jekyll/CAMBIAME-jekyll-theme. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty's code of
conduct](http://sutty.nl/en/code-of-conduct).

## License

The theme is available as open source under the terms of the [Antifacist
MIT License](LICENSE.txt).

